/*
* Полученное время разное, потому что при последовательном вызове функций
* в основном потоке программа выполняет каждую функцию последовательно,
* ожидая окончания выполнения каждой функции перед вызовом следующей.
*
* В асинхронном вызове функций
*  они начинают работать параллельно в фоновых потоках
*
* */

fun getNumber1(): Int {
    Thread.sleep(1000) // имитация задержки в 1 секунду
    return 1
}

fun getNumber2(): Int {
    Thread.sleep(2000) // имитация задержки в 2 секунды
    return 2
}

fun sleepMessage(i: Int) {
    Thread.sleep(1000)
    println("I'm sleeping $i...")
}


fun main() {
    for (i in 0..2) {
        sleepMessage(i)
    }
    println("main: I'm tired of waiting! I'm running finally")

    // последовательный вызов функций
    val startTimeSeq = System.currentTimeMillis()
    val num1 = getNumber1()
    val num2 = getNumber2()
    println("Sum: ${num1 + num2}")
    val endTimeSeq = System.currentTimeMillis()
    println("Time: ${endTimeSeq - startTimeSeq} ms")

    // асинхронный вызов функций
    val startTimeAsync = System.currentTimeMillis()
    var num1Async = 0
    var num2Async = 0
    val thread1 = Thread(Runnable {
        num1Async = getNumber1()
    })
    val thread2 = Thread(Runnable {
        num2Async = getNumber2()
    })
    thread1.start()
    thread2.start()
    thread1.join()
    thread2.join()
    println("Sum: ${num1Async + num2Async}")
    val endTimeAsync = System.currentTimeMillis()
    println("Time: ${endTimeAsync - startTimeAsync} ms")
    println("main: Now I can quit.")
}