fun main() {
    val thread = Thread {
        Thread.sleep(1000)
        println("World")
    }
    thread.start()

    Thread.sleep(2000)
    println("Hello")
    thread.join()
}